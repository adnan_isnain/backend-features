package main

import (
	"library"
	"log"

	"github.com/go-chi/chi/v5"
	"github.com/joho/godotenv"
)

func init() {
	// To be deleted, using docker-compose env not env file
	err := godotenv.Load()
	if err != nil {
		log.Fatal("Error loading .env file")
	}

	// Init all env checker, db, and internal service need to be connected
	library.CheckALLEnv()
}

func main() {
	s := HealthServer{
		port:   8080,
		router: chi.NewRouter(),
	}

	err := s.Run()
	if err != nil {
		panic(err)
	}
}
