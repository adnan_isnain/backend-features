package main

import (
	"fmt"
	"net/http"

	"github.com/go-chi/chi/v5"
)

type HealthServer struct {
	port   int
	router *chi.Mux
}

func (s *HealthServer) Run() error {
	return http.ListenAndServe(fmt.Sprintf("0.0.0.0:%d", s.port), s.router)
}
