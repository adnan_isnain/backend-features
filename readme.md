# Backend Features
This is mono project with multiple services. Services available
#### 1. REST API
    Directly connected to main API backend (CRUD API).
#### 2. AUTH API
    Handling AUTH API (registration, login, reset auth password, etc).
#### 3. BACKEND SERVICE
    Handling to queue, analytics to elastic, etc.

Will be developed gradually (soon).