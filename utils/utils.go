package utils

import (
	"os"
	"strconv"
)

func GetEnvString(key string) string {
	return os.Getenv(key)
}

func GetEnvInt(key string) int {
	val, _ := strconv.Atoi(GetEnvString(key))
	return val
}

func GetEnvBool(key string) bool {
	val, _ := strconv.ParseBool(GetEnvString(key))
	return val
}
