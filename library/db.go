package library

import (
	"database/sql"
	"fmt"
	"utils"
)

func InitSQLdb() *sql.DB {
	args := fmt.Sprintf(
		`host=%s port=%d user=%s password=%s dbname=%s sslmode=disable`,
		utils.GetEnvString(__DB_HOST__),
		utils.GetEnvInt(__DB_PORT__),
		utils.GetEnvString(__DB_USERNAME__),
		utils.GetEnvString(__DB_PASSWORD__),
		utils.GetEnvString(__DB_NAME__),
	)
	db, err := sql.Open("postgres", args)
	if err != nil {
		panic(err.Error())
	}

	err = db.Ping()
	if err != nil {
		panic(err.Error())
	}

	return db
}
