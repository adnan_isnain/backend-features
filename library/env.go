package library

import (
	"fmt"
	"log"
	"os"
	"strconv"
	"strings"
)

const (
	// Env key list name
	__DB_HOST__     = "DB_HOST"
	__DB_PORT__     = "DB_PORT"
	__DB_NAME__     = "DB_NAME"
	__DB_PASSWORD__ = "DB_PASSWORD"
	__DB_USERNAME__ = "DB_USERNAME"

	// Data type registered
	__string__  = "string"
	__integer__ = "integer"
	__boolean__ = "boolean"

	__true__  = "true"
	__false__ = "false"
)

var (
	// Enter env key and datatype registered here
	envLists = map[string]string{
		__DB_HOST__:     __string__,
		__DB_PORT__:     __integer__,
		__DB_NAME__:     __string__,
		__DB_PASSWORD__: __string__,
		__DB_USERNAME__: __string__,
	}
)

func CheckALLEnv() {
	var err error
	totalErr := 0

	for key, envType := range envLists {
		err = nil
		switch envType {
		case __string__:
			err = validateStringType(key)
		case __integer__:
			err = validateIntegerType(key)
		case __boolean__:
			err = validateBooleanType(key)
		default:
			totalErr += 1
			log.Printf(`Unknow data type %s (%s)`, key, envType)
		}

		if err != nil {
			totalErr += 1
			log.Println(err)
		}
	}

	if totalErr > 0 {
		log.Fatalln(`Stopping program, env key above cannot be blank and/or set with right type.`)
	}
}

func validateStringType(key string) error {
	if os.Getenv(key) == "" {
		return fmt.Errorf(`%s has not been set yet`, key)
	}

	return nil
}

func validateIntegerType(key string) error {
	if err := validateStringType(key); err != nil {
		return err
	}

	_, err := strconv.Atoi(os.Getenv(key))
	if err != nil {
		return fmt.Errorf(`%s must be integer`, key)
	}

	return nil
}

func validateBooleanType(key string) error {
	var err error
	if err = validateStringType(key); err != nil {
		return err
	}

	val := os.Getenv(key)
	switch strings.ToLower(val) {
	case "0", "false":
		_, err = strconv.ParseBool(__false__)
	case "1", "true":
		_, err = strconv.ParseBool(__true__)
	default:
		return fmt.Errorf(`%s must be boolean`, key)
	}

	if err != nil {
		return err
	}

	return nil
}
